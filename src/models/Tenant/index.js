
export default {
  first_name: { type: String },
  last_name: { type: String },
  age: { type: Number },
  email: { type: String },
};
