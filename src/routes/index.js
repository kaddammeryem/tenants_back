import { Router } from 'express';
import fs from 'fs';
import path from 'path';
import logger from '../utils/logger.js';
import { fileURLToPath } from 'url';

const basePath = process.pkg ? 
    path.dirname(fileURLToPath(import.meta.url)) 
    : 
    path.join(path.dirname(fileURLToPath(import.meta.url)), '../../src/routes')

export default async (app) => {
  await Promise.all(
    fs
      .readdirSync(basePath)
      .filter((routeName) => routeName !== 'index.js')
      .map(async (routeName) => {
        if (routeName !== 'index.js') {
          try {
            const router = Router();
            const { default: route } = await import(`./${routeName}/index.js`);
            route(router);
            app.use(`/${routeName}`, router);
          } catch (e) {
            logger.warn(`[ROUTING]: Cannot load ${routeName} route. ${e.message}`);
          }
        }
      })
  );
  app.use((req, res) => new Error(`url: '${req.originalUrl}' not found.`));
  logger.info('[SERVER]: Routes initialized.');
};
