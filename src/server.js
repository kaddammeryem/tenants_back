import express from 'express';
import http from 'http';
import bodyParser from 'body-parser';
import cors from 'cors';
import routes from './routes/index.js';
import logger from './utils/logger.js';
import mongo from './utils/mongo.js';
import swagger from './utils/swagger.js';

(async () => {
  const app = express();
  // The signals we want to handle
  const signals = {
    SIGHUP: 1,
    SIGINT: 2,
    SIGTERM: 15,
  };


  app.use(
    cors({
      methods: ['GET', 'POST', 'PUT', 'DELETE', 'PATCH'],
      origin: '*',
      exposedHeaders: 'X-Total-Count, X-Total-Result, Location, Content-Disposition',
      optionsSuccessStatus: 204,
    })
  );


  app.use(
    bodyParser.json({
      extended: true,
      limit: '15mb',
    })
  );

  await swagger(app);
  await mongo();
  await routes(app);

  const httpServer = http.createServer(app).listen(8000, () => {
    logger.info(`[SERVER]: Listening on: localhost`);
      if (process.pkg) logger.info(`[SERVER]: On Your local network: http://localhost:3000}`);
      logger.info(`[SERVER]: On Your local network: http://localhost:3000`);
  });

  // Do any necessary shutdown logic for our application here
  const shutdown = (signal, value) => {
    logger.info('shutdown!');
    httpServer.close(() => {
      logger.warn(`server stopped by ${signal} with value ${value}`);
      process.exit(128 + value);
    });
  };
  // Create a listener for each of the signals that we want to handle
  Object.keys(signals).forEach((signal) => {
    process.on(signal, () => {
      logger.warn(`process received a ${signal} signal`);
      shutdown(signal, signals[signal]);
    });
  });
})();
