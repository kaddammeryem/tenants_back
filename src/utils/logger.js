import { createLogger, format, transports } from 'winston';
import path from 'path';
import { fileURLToPath } from 'url';

const logLevel = process.env.LOG_LEVEL || 'debug';
const appDir = path
  .dirname(fileURLToPath(import.meta.url))
  .split('\\')
  .join('\\\\');

/**
 * Serialize the logging info object.
 *
 * @param {Object} info - The log information object to format.
 * @param {string} name - The name of the error/log.
 * @param {string} stack - The stacktrace of the error.
 * @param {number} statusCode - The status code of the error.
 * @param {string} [description] - The description of the error/log.
 * @param {string} timestamp - The actual timestamp.
 * @param {string} level - The log level.
 * @param {string} correlation_id - The correlation ID of the request.
 * @param {string} rawMessage - The message.
 * @returns {string}
 */
const serializeLog = ({
  name,
  stack,
  statusCode,
  description,
  timestamp,
  level,
  correlation_id,
  message: rawMessage,
}) => {
  const ts = timestamp.slice(0, 19).replace('T', ' ');
  const message = rawMessage[0] === '\t' ? rawMessage.substr(1) : rawMessage;

  // Simple logging.
  if (!name) return `${ts}${level}: ${message}`;

  const reg = new RegExp(`${appDir}(\\S+)`, 'gm');
  const match = reg.exec(stack);
  const location = match ? ` (at "${match[1]}")` : '';

  return `${ts} ${level}: [${statusCode} ${name || ''}]${location}${(correlation_id &&
    ` ${correlation_id}`) ||
    ''}: ${description || message}`;
};

/**
 * Format logging message, add color, timestamp, etc.
 */
const finalFormat = format.combine(
  format.colorize(),
  format.timestamp({
    format: 'DD-MM-YYYY HH:mm:ss',
  }),
  format.align(),
  format.json(),
  format.printf(serializeLog)
);
/**
 * Format logging message, add color, timestamp, etc.
 */
const finalProductionFormat = format.combine(
  format.timestamp({
    format: 'DD-MM-YYYY HH:mm:ss',
  }),
  format.json()
);

let logger;

if (process.env.NODE_ENV !== 'production') {
  logger = createLogger({
    level: logLevel,
    format: finalFormat,
    transports: [new transports.Console()],
  });
} else {
  logger = createLogger({
    level: logLevel,
    format: finalProductionFormat,
    defaultMeta: { service: process.env.API_NAME },
    transports: [new transports.Console()],
  });
}

export default logger;
