import mongoose from 'mongoose';
import 'dotenv/config'
import logger from './logger.js';

/**
 * Initialize the connection to the Mongo database.
 *
 * @returns {Promise<>}
 */
const mongo = async () => {
  if (!process.env.DB_NAME || !process.env.DB_PASSWORD || !process.env.DB_URL || !process.env.DB_USERNAME) {
    return logger.warn('[DATABASE]: Missing database settings.');
  }

  try {
    await mongoose.connect(`mongodb://admin:admin@${process.env.DB_URL}/${process.env.DB_NAME}`, {});
    console.log(`mongodb://${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@${process.env.DB_URL}/${process.env.DB_NAME}`);
    // await defaultSettings();
    logger.info('[DATABASE]: Default settings created.');
  } catch (e) {
    if (e.code === 11000) return logger.info('[DATABASE]: Default settings already created.');
    e.statusCode = 500;
    // logger.error(e);
   // return await mongo();
  }
  logger.info(`[SERVER]: Connected to the [${process.env.DB_NAME}](${process.env.DB_URL}) database.`);
};

export default mongo;