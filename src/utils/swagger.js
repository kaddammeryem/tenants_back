import swaggerUi from 'swagger-ui-express';
import swaggerJSDoc from 'swagger-jsdoc';

import { initModels } from '../models/index.js';

const options = {
  swaggerDefinition: {
    openapi: '3.0.0',
    basePath: '/',
    components: {
      securitySchemes: {
        BearerAuth: {
          type: 'http',
          scheme: 'bearer',
          bearerFormat: 'JWT',
        },
      },
    },
  },
  produces: ['application/json'],
  apis: ['src/routes/*/*'],
};

const routeName = 'api-docs';

/**
 * Setup, initialize all the Swagger services & also the Mongoose schemas & dynamic models.
 *
 * @param {Object} app - The Express app object
 * @returns {Promise<void>}
 */
export default async (app) => {
  options.swaggerDefinition.components.schemas = await initModels();

  const swaggerSpec = swaggerJSDoc(options);

  app.get(`/${routeName}.json`, (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    res.send(swaggerSpec);
  });

  app.use(`/${routeName}`, swaggerUi.serve, swaggerUi.setup(swaggerSpec));

 
};
